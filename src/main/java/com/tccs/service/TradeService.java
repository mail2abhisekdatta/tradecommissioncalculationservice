package com.tccs.service;

import java.util.List;

import com.tccs.dto.TradeDetailDTO;


public interface TradeService {

	public TradeDetailDTO saveTradeDetails(TradeDetailDTO tradeDetailDTO);
	
	
	public TradeDetailDTO getTradeDetailWithCommission(TradeDetailDTO tradeDetailDTO);
	
	
	public List<TradeDetailDTO> getMultiTradeDetailWithCommission(List<TradeDetailDTO> tradeDetailDTOs);
}
