CREATE TABLE commission_config (
    id   INTEGER      NOT NULL AUTO_INCREMENT,
    config_type VARCHAR(128) NOT NULL,
    com_per DECFLOAT,
    com_amt DECFLOAT,
    PRIMARY KEY (id)
);


CREATE TABLE trade_config (
    id   INTEGER      NOT NULL AUTO_INCREMENT,
    security_type INTEGER,
    transaction_type INTEGER,
    com_per DECFLOAT,
    com_amt DECFLOAT,
    max_trade_amt DECFLOAT,
    PRIMARY KEY (id)
);