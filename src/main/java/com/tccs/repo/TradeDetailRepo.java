package com.tccs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tccs.model.TradeDetailModel;

public interface TradeDetailRepo extends JpaRepository<TradeDetailModel,Long > {

}
