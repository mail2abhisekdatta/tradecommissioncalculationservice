INSERT INTO commission_config (config_type, com_per, com_amt) VALUES
  ('STO_BUY_COM_PER', 0.05, NULL),
  ('STO_SELL_COM_PER', 0.05, NULL),
  ('BON_BUY_COM_PER', 0.02, NULL),
  
  ('BON_SELL_COM_PER', 0.01, NULL),
  ('FX_BUY_COM_PER', 0.01, NULL),
  ('STO_SELL_MAX_TRADE_AMT_ADDED_COM', NULL, 500.0),
  ('FX_SELL_MAX_TRADE_AMT_COM', NULL, 1000.0);
  
  
  
INSERT INTO trade_config (security_type, transaction_type, com_per, com_amt, max_trade_amt) VALUES
  (0, 0, 0.05,  NULL, NULL),
  (0, 1, 0.05,  500, 1000000),
  (1, 0, 0.02,  NULL, NULL),
  (1, 1, 0.01,  NULL, NULL),
  (2, 0, 0.01,  NULL, NULL),
  (2, 1, NULL,  1000, 1000000);
  