package com.tccs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tccs.model.TradeConfigModel;
import com.tccs.util.SecurityType;
import com.tccs.util.TransactionType;

public interface TradeConfigRepo extends JpaRepository<TradeConfigModel, Integer>{

	
	public TradeConfigModel findBySecurityTypeAndTransactionType(SecurityType securityType, TransactionType transactionType);
}
