package com.tccs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tccs.dto.TradeDetailDTO;
import com.tccs.service.TradeService;

/*
 * Exposing API's for tread commission 
 * 
 * */
@RestController
public class TradeController {

	
	/*
	 * Injecting the trade service
	 * */
	@Autowired
	private TradeService tradeService;
	
	/*
	 * API for store trading info
	 * */
	@PostMapping("/tread/calculation")
	public ResponseEntity<?> saveTradeDetail(final @RequestBody TradeDetailDTO tradeDetailDTO) {
		return ResponseEntity.status(HttpStatus.OK).body(tradeService.saveTradeDetails(tradeDetailDTO));
	}
	
	
	/*
	 * API for commission calculation for single trade
	 * */
	@PostMapping("/tread/commission")
	public ResponseEntity<?> getCommission(final @RequestBody TradeDetailDTO tradeDetailDTO) {
		return ResponseEntity.status(HttpStatus.OK).body(tradeService.getTradeDetailWithCommission(tradeDetailDTO));
	}
	
	/*
	 * 
	 * API for commission calculation for multiple trades
	 * */
	@PostMapping("/treads/commission")
	public ResponseEntity<?> getCommissions(final @RequestBody List<TradeDetailDTO> tradeDetailDTOs) {
		return ResponseEntity.status(HttpStatus.OK).body(tradeService.getMultiTradeDetailWithCommission(tradeDetailDTOs));
	}

	
}
