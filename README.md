
API for Single trade
--------------------------------
  URI : http://localhost:8080/tread/calculation
  method: POST

For Single trade JSON format will be
-----------------------------------------
{
    "treadTime": "01/01/2019 10:00:00",
    "securityType": "FX",
    "transactionType": "SELL",
    "quantity": 100000,
    "price": 12
}

API for Single trade
--------------------------------
  URI : http://localhost:8080/treads/calculation
  method: POST

For multiple trade JSON format will be 
-------------------------------------------
[{
    "treadTime": "01/01/2019 10:00:00",
    "securityType": "STO",
    "transactionType": "SELL",
    "quantity": 100000,
    "price": 12
},
{
    "treadTime": "01/01/2019 10:00:00",
    "securityType": "FX",
    "transactionType": "BUY",
    "quantity": 100000,
    "price": 12
}
]

