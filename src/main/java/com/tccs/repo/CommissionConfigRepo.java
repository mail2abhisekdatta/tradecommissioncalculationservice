package com.tccs.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tccs.model.CommissionConfigModel;

public interface CommissionConfigRepo extends JpaRepository<CommissionConfigModel,Integer> {

	
	public CommissionConfigModel findByConfigType(final String configType);
}
