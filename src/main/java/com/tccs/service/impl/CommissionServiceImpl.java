package com.tccs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tccs.dto.TradeDetailDTO;
import com.tccs.model.TradeConfigModel;
import com.tccs.repo.TradeConfigRepo;
import com.tccs.service.CommissionService;

@Service("commissionService")
public class CommissionServiceImpl implements CommissionService{

    
    @Autowired
    private TradeConfigRepo tradeConfigRepo;
    
    @Override
    public Double calculateCommission(TradeDetailDTO tradeDetailDTO) {
    	Double commission = 0.0;
    	
    	/*Calculating trade value*/
		Double tradeAmount = tradeDetailDTO.getPrice() * tradeDetailDTO.getQuantity();
		
		/*Getting commission configuration from DB*/
		TradeConfigModel tradeConfigModel = tradeConfigRepo.findBySecurityTypeAndTransactionType(tradeDetailDTO.getSecurityType(), tradeDetailDTO.getTransactionType());
		
		//Validate trade config exists based on security and transaction type
		if(null!=tradeConfigModel) {
			//Validate commission percentage exists for the config
			if(null!=tradeConfigModel.getCommmisionPercentage()) {
				// Calculate commission based on config
				commission = tradeAmount * (tradeConfigModel.getCommmisionPercentage()/100);
				
				//Validate trade amount exceed configured max trade amount
				if(null!=tradeConfigModel.getMaxTradeAmount() && tradeAmount > tradeConfigModel.getMaxTradeAmount()) {
					// Add advisory or other commission amount
					commission = commission + tradeConfigModel.getCommissionAmount();
				}
			} else {
				//If commission percentage not define for FX and SELL
				commission = tradeConfigModel.getCommissionAmount();
			}
		}
		return commission;
    }
}
