package com.tccs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradecommissioncalculationserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradecommissioncalculationserviceApplication.class, args);
	}

}
