package com.tccs.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tccs.util.SecurityType;
import com.tccs.util.TransactionType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TradeDetailDTO {

	private Long id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date treadTime;
	private SecurityType securityType;
	private TransactionType transactionType;
	private Integer quantity;
	private Double price;
	private Double commisionAmount;
	
}
