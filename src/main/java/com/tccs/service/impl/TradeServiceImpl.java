package com.tccs.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tccs.dto.TradeDetailDTO;
import com.tccs.model.TradeDetailModel;
import com.tccs.repo.TradeDetailRepo;
import com.tccs.service.CommissionService;
import com.tccs.service.TradeService;
import com.tccs.util.Commission;

/*
 * Service layer for trade commission business operations
 * 
 * */
@Service("tradeService")
public class TradeServiceImpl implements TradeService{
	
	@Autowired
	private TradeDetailRepo tradeDetailRepo;
	
	
	//One way to calculate commission
	@Autowired
	private Commission comService;
	
	//Second way to calculate commission
	@Autowired
	private CommissionService commissionService;
	
	/*
	 * 
	 * Service method used to convert DTO to Model and persist data
	 * */
	@Override
	public TradeDetailDTO saveTradeDetails(TradeDetailDTO tradeDetailDTO) {
		
		/*
		 * Copy data from DTO to Model
		 * */
		TradeDetailModel tradeDetailModel =new TradeDetailModel();
		BeanUtils.copyProperties(tradeDetailDTO, tradeDetailModel);
		
		/*
		 * Repository call to persist trade data
		 * */
		tradeDetailModel = tradeDetailRepo.save(tradeDetailModel);
		
		/*
		 * Copy data from Model to DTO
		 * */
		BeanUtils.copyProperties(tradeDetailModel, tradeDetailDTO);
		return tradeDetailDTO;
		
	}
	
	/*
	 * Service method to calculate and build trade detail with commission
	 * 
	 * */
	@Override
	public TradeDetailDTO getTradeDetailWithCommission(TradeDetailDTO tradeDetailDTO) {
		Double commission = comService.calculateCommission(tradeDetailDTO);
    	tradeDetailDTO.setCommisionAmount(commission);
    	return tradeDetailDTO;
	}
	

	/*
	 * Service method to calculate and build multiple trade details with commission
	 * 
	 * */
	@Override
	public List<TradeDetailDTO> getMultiTradeDetailWithCommission(List<TradeDetailDTO> tradeDetailDTOs) {
		return tradeDetailDTOs.parallelStream()
							  .map(tradeDetailDTO -> {
								  Double commission = commissionService.calculateCommission(tradeDetailDTO);
								  tradeDetailDTO.setCommisionAmount(commission);
							      return tradeDetailDTO;
							  })
							  .collect(Collectors.toList());
	}
	

}
