package com.tccs.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tccs.dto.TradeDetailDTO;
import com.tccs.model.CommissionConfigModel;
import com.tccs.repo.CommissionConfigRepo;
import com.tccs.service.CommissionService;

@Service("comService")
public class Commission implements CommissionService{
	
    private static Double STO_SELL_MAX_TRADE_AMT = 100000.0;
    private static Double FX_SELL_MAX_TRADE_AMT = 1000000.0;
    
    private static String STO_BUY_COM_PER = "STO_BUY_COM_PER";
    private static String STO_SELL_COM_PER = "STO_SELL_COM_PER";
    private static String BON_BUY_COM_PER = "BON_BUY_COM_PER";
    private static String BON_SELL_COM_PER = "BON_SELL_COM_PER";
    private static String FX_BUY_COM_PER = "FX_BUY_COM_PER";
    
    
    private static String STO_SELL_MAX_TRADE_AMT_ADDED_COM = "STO_SELL_MAX_TRADE_AMT_ADDED_COM";
    private static String FX_SELL_MAX_TRADE_AMT_COM = "FX_SELL_MAX_TRADE_AMT_COM";
    
    @Autowired
    private CommissionConfigRepo commissionConfigRepo;
    
    
    public Double calculateCommission(TradeDetailDTO tradeDetailDTO) {
		Double commission = 0.0;
		Double tradeAmount = tradeDetailDTO.getPrice() * tradeDetailDTO.getQuantity();
		
		switch (tradeDetailDTO.getTransactionType()) {
		case BUY:
			switch (tradeDetailDTO.getSecurityType()) {
				case STO:
					CommissionConfigModel configModelSTO = commissionConfigRepo.findByConfigType(STO_BUY_COM_PER);
					commission = tradeAmount * (configModelSTO.getCommmisionPercentage()/100);
					break;
				case BON:
					CommissionConfigModel configModelBON = commissionConfigRepo.findByConfigType(BON_BUY_COM_PER);
					commission = tradeAmount * (configModelBON.getCommmisionPercentage()/100);
					break;
				case FX:
					CommissionConfigModel configModelFX = commissionConfigRepo.findByConfigType(FX_BUY_COM_PER);
					commission = tradeAmount * (configModelFX.getCommmisionPercentage()/100);
					break;
			}
			break;
		case SELL:
			switch (tradeDetailDTO.getSecurityType()) {
				case STO:
					CommissionConfigModel configModelSTO = commissionConfigRepo.findByConfigType(STO_SELL_COM_PER);
					commission = tradeAmount * (configModelSTO.getCommmisionPercentage()/100);
					if(tradeAmount > STO_SELL_MAX_TRADE_AMT) {
						CommissionConfigModel configModel =commissionConfigRepo.findByConfigType(STO_SELL_MAX_TRADE_AMT_ADDED_COM);
						commission = commission + configModel.getCommissionAmount();
					}
					break;
				case BON:
					CommissionConfigModel configModelBON = commissionConfigRepo.findByConfigType(BON_SELL_COM_PER);
					commission = tradeAmount * (configModelBON.getCommmisionPercentage()/100);
					break;
				case FX:
					CommissionConfigModel configModelFX = commissionConfigRepo.findByConfigType(FX_SELL_MAX_TRADE_AMT_COM);
					if(tradeAmount > FX_SELL_MAX_TRADE_AMT) {
						commission = configModelFX.getCommissionAmount();
					}
					break;
					
			}
			break;
	}
		return commission;
	}
    

}
