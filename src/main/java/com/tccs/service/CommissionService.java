package com.tccs.service;

import com.tccs.dto.TradeDetailDTO;

public interface CommissionService {

	public Double calculateCommission(TradeDetailDTO tradeDetailDTO);
}
